import json
import pprint
import MySQLdb

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, HiveContext

# pylint: disable-msg=C0103
with open('../config.json', 'r') as configFile:
    config = json.load(configFile)
 
def readFromFile(fileName):
    "This method read data from given filename"
    data = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load(fileName)
    return data

def writeDataToMySQL(data):
    "This method writes data to mysql db"
    sdf_props = {'user':'root','password':'rgukt123', 'driver':'com.mysql.jdbc.Driver'}
    data.write.jdbc(
    	url='jdbc:mysql://localhost/test',
    	table='persons',
    	mode='append',
    	properties = sdf_props
    )
    print "Data saved into MySQL database successfully"

def main():
    data = readFromFile(config["FILE_PATH1"])
    writeDataToMySQL(data)
    

if __name__ == "__main__":
    #Spark Configuration
    # pylint: disable-msg=C0103
    conf = SparkConf().setAppName("data-ingestion")
    conf = conf.setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    hiveContext = HiveContext(sc)
    #Execute main function
    main()
