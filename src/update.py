import json
import pprint
import MySQLdb
import os

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, HiveContext

# pylint: disable-msg=C0103
with open('../config.json', 'r') as configFile:
    config = json.load(configFile)  
   
def loadFromMysql():
    data = hiveContext.read.format("jdbc").options(
        url=config["DATABASE"]["url"],
        driver=config["DATABASE"]["driver"],
        user=config["DATABASE"]["user"],
        password=config["DATABASE"]["password"],
        dbtable=config["DATABASE"]["dbtable"]
    ).load()

    return data

def createIncrementalTable():
    sqoopcom = "sqoop import --connect jdbc:mysql://localhost:3306/test --username root --password rgukt123  --split-by id --table customers --check-column updated_time --incremental lastmodified --last-value 2017-09-25 --target-dir /user/root/inc_tbl1"
    os.system(sqoopcom)

def createView():
    hiveContext.sql('use test')
    hiveContext.sql('DROP VIEW rc_view2')
    sql = 'create view rc_view2 as select t3.*, t2.name from (SELECT * FROM customers UNION ALL SELECT * FROM inc_tbl1) as t2 join (SELECT t1.id, max(t1.updated_time) as max_date FROM (select * from customers union all select * from inc_tbl1) as t1 group by t1.id) as t3 on t2.id=t3.id and t2.updated_time=t3.max_date'
    #hiveQl = "CREATE EXTERNAL TABLE inc_tbl1 (id varchar(200), name varchar(200), updated_time timestamp) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LOCATION '/user/root/inc_tbl1'"
    #hiveContext.sql(hiveQl)
    hiveContext.sql(sql)
    hiveData1 = hiveContext.sql('select * from rc_view2')
    hiveContext.sql('DROP TABLE reporting_table')
    hiveContext.sql('CREATE TABLE reporting_table AS SELECT * FROM rc_view2')
    hiveContext.sql('DROP TABLE customers')
    hiveContext.sql('CREATE TASCHEMABLE customers AS SELECT * FROM reporting_table')
    hiveData1.show()
    print '---------------------------------------------------'

def insertToHive(mysqlData):
    newDf = mysqlData
    newDf.write.format("orc").saveAsTable("employee")
    hiveData = hiveContext.sql("SELECT * FROM employee")
    hiveData.show()

def main():
    createIncrementalTable()
    createView()
    #mysqlData = loadFromMysql()
    #insertToHive(mysqlData)
    
  
    #insertToHive(mysqlData)
if __name__ == "__main__":
    #Spark Configuration
    # pylint: disable-msg=C0103
    conf = SparkConf().setAppName("data-ingestion")
    conf = conf.setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    hiveContext = HiveContext(sc)
    #Execute main function
    main()
