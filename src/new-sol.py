import json
import pprint
import MySQLdb

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, HiveContext

# pylint: disable-msg=C0103
with open('../config.json', 'r') as configFile:
    config = json.load(configFile)

def insertToHive(mysqlData):
    newDf = mysqlData
    newDf.show()
    newDf.write.mode("overwrite").format("orc").saveAsTable("employee")
  
def loadFromHive():
    hiveContext.sql("create table if not exists employees(id int, name varchar(250),email varchar(250), address varchar(250), created_time timestamp, updated_time timestamp)")
    hiveData = hiveContext.sql("SELECT * FROM employees")
    return hiveData  
   
def loadFromMysql():
    data = hiveContext.read.format("jdbc").options(
        url=config["DATABASE"]["url"],
        driver=config["DATABASE"]["driver"],
        user=config["DATABASE"]["user"],
        password=config["DATABASE"]["password"],
        dbtable=config["DATABASE"]["dbtable"]
    ).load()

    return data

def updateInHive(upsertDF):
    upsertDF.write.mode("overwrite").format("orc").saveAsTable("upserts_tb")

def deleteInHive(deletesDF):
    deletesDF.write.mode("overwrite").format("orc").saveAsTable("deletes_tbl")
    hiveContext.sql("DELETE FROM employees WHERE EXISTS (SELECT * FROM deletes_tbl WHERE employee.id = deletes_tbl.id)")  
    hiveData = loadFromHive() 

def finalHiveData(updsertsDF):
    hiveContext.sql("INSERT INTO TABLE employees SELECT * FROM upserts_tb")  
    hiveData = loadFromHive()
    hiveData.show()
    print '-------------------------------final'

def main():
    # old data
    mysqlData = loadFromMysql()
    insertToHive(mysqlData)

    # updated data
    mysqlData = loadFromMysql()

    hiveData = loadFromHive()
    upsertsDF = mysqlData.subtract(hiveData)
    updateInHive(upsertsDF) 
    
    deletesDF = hiveData.subtract(mysqlData) 
    #deleteInHive(deletesDF)
    
    finalHiveData(upsertsDF)
if __name__ == "__main__":
    #Spark Configuration
    # pylint: disable-msg=C0103
    conf = SparkConf().setAppName("data-ingestion")
    conf = conf.setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    hiveContext = HiveContext(sc)
    #Execute main function
    main()
