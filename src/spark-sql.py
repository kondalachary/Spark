import json
import pprint
import MySQLdb
import os
import schedule
import time
from time import gmtime, strftime

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, HiveContext

# pylint: disable-msg=C0103
with open('../config.json', 'r') as configFile:
    config = json.load(configFile)  

def getLastModifiedTime():
    lastUpdatedTime = hiveContext.sql('select max(updated_time) as updated_time from base_table')
    return lastUpdatedTime.collect()[0]['updated_time']
   
def importMysqlToHive():
    last_max_modified_date = getLastModifiedTime()
    if last_max_modified_date is None:
       last_max_modified_date = '0'

    data = hiveContext.read.format("jdbc").options(
        url="jdbc:mysql://localhost/test",
        driver="com.mysql.jdbc.Driver",
        user="root",
        password="rgukt123",
        dbtable="(select * from customers where updated_time>'"+str(last_max_modified_date)+"') cust_alias"
    ).load()

    data.write.mode('append').format("orc").saveAsTable("base_table")

def createExternalTable():
    hiveQl = "CREATE EXTERNAL TABLE IF NOT EXISTS base_table (id varchar(200), name varchar(200), updated_time timestamp) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LOCATION '/user/root/base_table'"
    hiveContext.sql(hiveQl)

def createView():
    hiveContext.sql('drop view reconcile_view')

    hiveQl2 = "create view reconcile_view as select t1.* from base_table as t1 join (select id, max(updated_time) as updated_time from base_table group by id) as t2 on t1.id=t2.id where t1.updated_time=t2.updated_time"
    hiveContext.sql(hiveQl2)

def main():
    hiveContext.sql('use test')	
    createExternalTable()
    importMysqlToHive()
    createView()
    #excuteJob()
    #schedule.every(2).minutes.do(excuteJob)
    #createExternalTable()

if __name__ == "__main__":
    #Spark Configuration
    # pylint: disable-msg=C0103
    conf = SparkConf().setAppName("data-ingestion")
    conf = conf.setMaster("local[*]")
    sc = SparkContext(conf=conf)
    sqlContext = SQLContext(sc)
    hiveContext = HiveContext(sc)
    #Execute main function
    main()
